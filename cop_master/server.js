const express = require('express');
const path = require('path');
const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views/pages'));
app.use(express.static(path.join(__dirname, 'public')));
app.set('view engine', 'ejs');

app.get("/", (req, res) => {
    res.render("index")
})
app.listen(8080, (err) => {
    if (err) {
        console.log(`Server error: ${err}`);
    } else {
        console.log("Server running...");
    }
})